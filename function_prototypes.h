#ifndef FUNCTION_PROTOTYPES  // Include guard
#define FUNCTION_PROTOTYPES
#include <stdint.h>


typedef struct WAV_HEADER {
    unsigned char chunk_id[4];
    uint32_t chunk_size;
    unsigned char format[4];

    unsigned char subchunk1_id[4];
    uint32_t subchunk1_size;
    uint16_t audio_format;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;

    unsigned char subchunk2_id[4];
    uint32_t subchunk2_size;
} __attribute__((packed)) Wav;  // __attribute__((packed)) packs all members - disables padding 

//-------- dscanf ----------
// DESCRIPTION:
//  - Dynamic "scanf" function. Reads user input from console in a dynamically
//    allocated array/buffer
// PARAMETERS:
//  - No parameters
// RETURN VALUE:
//  - Returns a char pointer to the the user input string
//------------------
char * dscanf(void);

//-------- is_valid_filetype ----------
// DESCRIPTION:
//  - Function, that checks whether a file, read in a Wav struct, is .wav file.
//    This is achieved by checking if chunk_id == "RIFF" and format == "WAVE"
// PARAMETERS:
//  - Struct Wav instance, containing file information
// RETURN VALUE:
//  - Returns an int representing TRUE(1) or FALSE(0)
//------------------
int is_valid_filetype(const Wav wav);

//-------- get_duration_by_bytes ----------
// DESCRIPTION:
//  - Function to calculate duration of .wav file in seconds by it's header details
// PARAMETERS:
//  - Struct Wav instance, containing file information
// RETURN VALUE:
//  - Returns an unsigned int, equal to the duration of the .wav file (in seconds)
//------------------
unsigned int get_duration_by_bytes(const Wav wav);

//-------- get_bytes_by_duration ----------
// DESCRIPTION:
//  - Function to calculate how much DATA header section bytes are needed
//    for a given .wav file to be "int duration" seconds long (for .wav file)
// PARAMETERS:
//  - Struct Wav instance, containing file information
//  - int duration - integer value of desired seconds of duration
// RETURN VALUE:
//  - Returns a long int, equal to how much DATA header section bytes are needed
//    for a given .wav file to be "int duration" seconds long
//------------------
long get_bytes_by_duration(const Wav wav, const int duration);

//-------- char* get_wav_duration_text(Wav wav) ----------
// DESCRIPTION:
//  - Function that generates a duration print message in <hh>:<mm>:<ss> format for a given .wav file
// PARAMETERS:
//  - Struct Wav instance, containing file information
// RETURN VALUE:
//  - Returns a char pointer to the the duration print message string
//------------------
char* get_wav_duration_text(const Wav wav);

//-------- print_wav_info ----------
// DESCRIPTION:
//  - Function to print header information for a .wav file, including it's duration
// PARAMETERS:
//  - Struct Wav instance, containing file information
// RETURN VALUE:
//  - No return value
//------------------
void print_wav_info(const Wav wav);

//-------- get_original_filename ----------
// DESCRIPTION:
//  - Function that gets a filename (without extension) by a given file_path
//    (Without file descriptor, because on Windows)
// PARAMETERS:
//  - char pointer to "file_path" string, containing absolute path to a file
// RETURN VALUE:
//  - Returns a char pointer to the the extracted filename string
//------------------
char* get_original_filename(const char* const file_path);

//------- get_new_filename -----------
// DESCRIPTION:
//  - Function that gets a new filename for modified files (with extension) by a given file_path
//    (contains "-modified")
// PARAMETERS:
//  - char pointer to a filename (without extension) string
// RETURN VALUE:
//  - Returns a char pointer to the generated new filename
//------------------
char* get_new_filename(const char* const original_filename);

//-------- copy_all_file_contents ----------
// DESCRIPTION:
//  - Function, used to copy all file contents from source to destination, using buffer
// PARAMETERS:
//  - FILE* source - file pointer of the source file
//  - FILE* destination - file pointer of the destination file
// RETURN VALUE:
//  - Returns a long int, equal to the count of copied bytes
//------------------
long copy_all_file_contents(FILE* const source, FILE* const destination);

//-------- data_section_bytes_copy ----------
// DESCRIPTION:
//  - Function, used to copy "target_bytes" bytes from source file's DATA header section to
//    destination file's DATA header section using buffer
// PARAMETERS:
//  - FILE* source - file pointer of the source file
//  - FILE* destination - file pointer of the destination file
//  - int wav_header_size - Size of Wav struct instance 
//  - int target_bytes - number of bytes to be copied
// RETURN VALUE:
//  - Returns a long int, equal to the count of copied bytes
//------------------
long data_section_bytes_copy(FILE* const source, FILE* const destination, const int wav_header_size, const int target_bytes);

//------- update_wav_header_sizes -----------
// DESCRIPTION:
//  - Function to update .wav file's header duration and size information
// PARAMETERS:
//  - FILE* fp - file pointer of the wav file
//  - int* new_chunk_size - int pointer to the new chunk_size value
//  - int* new_subchunk2_size - int pointer to the new subchunk2_size value
// RETURN VALUE:
//  - No return value
//------------------
void update_wav_header_sizes(FILE* const fp, const int* const new_chunk_size, const int* const new_subchunk2_size);

//------- change_wav_duration -----------
// DESCRIPTION:
//  - Function to change a .wav file's duration and update it's filename
// PARAMETERS:
//  - Wav* wav_header - Struct Wav pointer, pointing to file information
//  - char* file_path - char pointer to an absolute filepath to a file
//  - int current_duration - Current duration of .wav file in seconds
//  - int new_duration - Desired new duration of .wav file in seconds
// RETURN VALUE:
//  - No return value
//------------------
void change_wav_duration(const Wav* const wav_header, const char* const file_path, const int current_duration, const int new_duration);

#endif
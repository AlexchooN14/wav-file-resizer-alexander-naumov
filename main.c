#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "function_prototypes.h"

extern int overwrite;
extern int silent;

int main(int argc, char* argv[]) {
    char* file_path;
    FILE* wav_file;
    Wav wav;
    char option;
    int new_duration = 0;

    while ((option = getopt(argc, argv, "os")) != -1) {
        switch (option) {
            case 'o':
                overwrite = 1;
                break;
            case 's':
                silent = 1;
                break;
        }
    }

    (!silent) ? printf("Enter full path to .wav file: \n") : 0;
    file_path = dscanf();
    
    wav_file = fopen(file_path, "rb");
    if (wav_file == NULL) {
        perror("Error opening file");
        free(file_path);
        exit(1);
    }

    fread(&wav, 1, sizeof(wav), wav_file);
    fclose(wav_file);
    if (!is_valid_filetype(wav)) {
        (!silent) ? printf("Invalid file type\n") : 0;
        free(file_path);
        exit(-1);
    }
    (!silent) ? print_wav_info(wav) : 0;

    int current_duration = get_duration_by_bytes(wav);

    (!silent) ? printf("Enter new duration of wav: \n") : 0;
    scanf("%d", &new_duration);
    change_wav_duration(&wav, file_path, current_duration, new_duration);
    free(file_path);

    return 0;
}

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "function_prototypes.h"
#include "constants.h"
#define DURATION_TEXT "The length of the file content is "
#define MALLOC_ERROR_MESSAGE "Error when malloc"
#define WAV_ERROR_MESSAGE "Open new .wav file error"

// TODO WHEN ERRORS OF MALLOC CHECK IF OTHER MEMORY WAS ALLOCATED BEFORE 

char* dscanf() {
    char c;
    int counter = 0;
    char* buffer = NULL;

    fflush(stdin);

    buffer = (char *) malloc(BUF_SIZE * sizeof(char));
    if (buffer == NULL) {
        perror(MALLOC_ERROR_MESSAGE);
        exit(-1);
    }

    do {
        if (counter == BUF_SIZE) {
            buffer = (char *) realloc(buffer, (sizeof(buffer) + (BUF_SIZE * sizeof(char))));
        }
        c = fgetc(stdin);
        buffer[counter] = c;
        counter++;

    } while(c != '\n' && !feof(stdin));

    buffer[counter - 1] = '\0';  // Replacing new line with \0
    return buffer;
}

int is_valid_filetype(const Wav wav) {
    return wav.chunk_id[0] == 'R' && wav.chunk_id[1] == 'I' && wav.chunk_id[2] == 'F' && wav.chunk_id[3] == 'F' &&
           wav.format[0] == 'W' && wav.format[1] == 'A' && wav.format[2] == 'V' && wav.format[3] == 'E'; 
}

unsigned int get_duration_by_bytes(const Wav wav) {
    return wav.subchunk2_size / (wav.sample_rate * (unsigned int)wav.num_channels * (unsigned int)wav.bits_per_sample / 8);
}

long get_bytes_by_duration(const Wav wav, const int duration) {
    return wav.sample_rate * (unsigned int)wav.num_channels * (unsigned int)wav.bits_per_sample * duration / 8;
}

char* get_wav_duration_text(const Wav wav) {
    char* str = NULL;
    int i = 0;

    unsigned int duration = get_duration_by_bytes(wav);
    int hours = duration / 3600;
    int minutes = (duration - (hours * 3600)) / 60;
    int seconds = (duration - (hours * 3600) - (minutes * 60));
    int time_arr[3] = {hours, minutes, seconds};
    
    if (duration == 0) {
        str = (char*) malloc((strlen(DURATION_TEXT) + 2) * sizeof(char));
        if (str == NULL) {
            perror(MALLOC_ERROR_MESSAGE);
            exit(-1);
        }
        strcat(str, DURATION_TEXT);
        strcat(str, "0");
        return str;
    }

    str = (char*) malloc((strlen(DURATION_TEXT) + strlen("00:00:00") + 1) * sizeof(char));
    if (str == NULL) {
        perror(MALLOC_ERROR_MESSAGE);
        exit(-1);
    }

    strcpy(str, DURATION_TEXT);

    for (i = 0; i < 3; i++) {   // Made for the sake of not using too much libraries
        if (time_arr[i] < 1) {
            strcat(str, "00");
        } else {
            char temp[4];
            if (time_arr[i] < 10) {
                sprintf(temp, "0%d", time_arr[i]);
            } else {
                sprintf(temp, "%d", time_arr[i]);
            }
            strcat(str, temp);
        }
        if (i != 2) strcat(str, ":");
    }

    return str;
}

void print_wav_info(const Wav wav) {
    char* duration_text = get_wav_duration_text(wav);
    printf("Chunk_id \t%c%c%c%c\n", wav.chunk_id[0], wav.chunk_id[1], wav.chunk_id[2], wav.chunk_id[3]);
    printf("Chunk_size \t%d\n", wav.chunk_size);
    printf("Format \t\t%c%c%c%c\n", wav.format[0], wav.format[1], wav.format[2], wav.format[3]);
    printf("\n");
    printf("Subchunk1_id \t%c%c%c%c\n", wav.subchunk1_id[0], wav.subchunk1_id[1], wav.subchunk1_id[2], wav.subchunk1_id[3]);
    printf("Subchunk1_size \t%d\n", wav.subchunk1_size);
    printf("AudioFormat \t%u\n", (unsigned int)wav.audio_format);
    printf("NumChannels \t%u\n", (unsigned int)wav.num_channels);
    printf("SampleRate \t%d\n", wav.sample_rate);
    printf("ByteRate \t%d\n", wav.byte_rate);
    printf("BlockAlign \t%u\n", (unsigned int)wav.block_align);
    printf("BitsPerSample \t%u\n", (unsigned int)wav.bits_per_sample);
    printf("\n");
    printf("Subchunk2_id \t%c%c%c%c\n", wav.subchunk2_id[0], wav.subchunk2_id[1], wav.subchunk2_id[2], wav.subchunk2_id[3]);
    printf("Subchunk2_size \t%d\n", wav.subchunk2_size);
    printf("\n");
    printf("%s\n", duration_text);
    free(duration_text);
}

// char* get_original_filename(char* file_path) {  // Only Linux
//     FILE* fp;
//     fp = fopen(file_path, "rb");
//     if (fp == NULL) {
//         perror(".wav file error:");
//         exit(1);
//     }
//     int file_desc = fileno(fp);
//
//     char* filename = malloc(BUF_SIZE * sizeof(char));
//     int result = fcntl(file_desc, F_GETPATH, filename);
//     if (result == -1) {
//         printf("Unable to get file path\n");
//         exit(1);
//     }
//
//     fclose(fp);
//     return filename;
// }

char* get_original_filename(const char* const file_path) {
    char* last_separator_occurence;
    char* filename;

    last_separator_occurence = strrchr(file_path, '/');
    if (last_separator_occurence == NULL) {
        last_separator_occurence = strrchr(file_path, '\\');
        if (last_separator_occurence == NULL) {
            return NULL;
        }
    }
    
    filename = (char*) malloc(strlen(last_separator_occurence + 1));
    if (filename == NULL) {
        perror(MALLOC_ERROR_MESSAGE);
        exit(-1);
    }
    memcpy(filename, last_separator_occurence + 1, strlen(last_separator_occurence + 1));
    last_separator_occurence = NULL;
    last_separator_occurence = strrchr(filename, '.');

    if (last_separator_occurence) {
        *last_separator_occurence = '\0';
    }

    return filename;
}

char* get_new_filename(const char* const original_filename) {
    char* new_filename = (char *) malloc((strlen(original_filename) + strlen("-modified.wav") + 1) * sizeof(char));
    if (new_filename == NULL) {
        perror(MALLOC_ERROR_MESSAGE);
        exit(-1);
    }
    strcpy(new_filename, original_filename);
    strcat(new_filename, "-modified.wav");
    strcat(new_filename, "\0");
    
    return new_filename;
}

long copy_all_file_contents(FILE* const source, FILE* const destination) {
    char buffer[BUF_SIZE];
    long bytes_done = 0;
    int bytes = 0;

    fseek(source, 0, SEEK_SET);        // Go to the start of the source file
    fseek(destination, 0, SEEK_SET);   // Go to the start of the destination file

    while ((bytes = fread(buffer, 1, BUF_SIZE, source)) > 0) {
        fwrite(buffer, 1, bytes, destination);
        bytes_done += bytes;
    }
    
    return bytes_done;
}

long data_section_bytes_copy(FILE* const source, FILE* const destination, const int wav_header_size, const int target_bytes) {
    char buffer[BUF_SIZE];
    long bytes_done = 0;
    int bytes = 0;

    fseek(source, wav_header_size, SEEK_SET);        // Go to the data section of the source file
    fseek(destination, wav_header_size, SEEK_SET);   // Go to the data section of the destination file

    while (bytes_done < target_bytes) {
        long bytes_remaining = target_bytes - bytes_done;
        if (bytes_remaining > BUF_SIZE) {
            bytes_remaining = BUF_SIZE;
        }
        bytes = fread(buffer, 1, bytes_remaining, source);
        if (bytes == 0) {
            break;
        }
        fwrite(buffer, 1, bytes, destination);
        bytes_done += bytes;
    }

    return bytes_done;
}

void update_wav_header_sizes(FILE* const fp, const int* const new_chunk_size, const int* const new_subchunk2_size) {
    fseek(fp, sizeof(unsigned char) * 4, SEEK_SET);       // Go to the ChunkSize position in header
    fwrite(new_chunk_size, sizeof(uint32_t), 1, fp);     // Write new chunk size in new file header
    fseek(fp, 40, SEEK_SET);                              // Go to the Subchunk2_size position in header
    fwrite(new_subchunk2_size, sizeof(uint32_t), 1, fp); // Write new chunk size in new file header
}

void change_wav_duration(const Wav* const wav_header, const char* const file_path, const int current_duration, const int new_duration) {
    int new_subchunk2_size = 0;
    int new_chunk_size = 0;
    long target_bytes = get_bytes_by_duration(*wav_header, new_duration);
    char* original_filename = get_original_filename(file_path);  // Without the .wav extension
    char* new_filename = get_new_filename(original_filename);    // With the .wav extension
    FILE *old_wav, *new_wav;

    old_wav = fopen(file_path, "rb+");
    if (old_wav == NULL) {
        perror(WAV_ERROR_MESSAGE);
        free(original_filename);
        free(new_filename);
        return;
    }

    new_wav = fopen(new_filename, "wb+");
    if (new_wav == NULL) {
        perror(WAV_ERROR_MESSAGE);
        fclose(old_wav);
        free(original_filename);
        free(new_filename);
        return;
    }

    fseek(old_wav, 0, SEEK_SET);    // Go to the start of the old file
    fseek(new_wav, 0, SEEK_SET);    // Go to the start of the new file
    fwrite(wav_header, 1, sizeof(wav_header), new_wav);  // Write old file header information to new file

    if (new_duration < current_duration) {
        data_section_bytes_copy(old_wav, new_wav, sizeof(wav_header), target_bytes);

        new_subchunk2_size = target_bytes;         // Decrease data chunk size
        new_chunk_size = 36 + new_subchunk2_size;  // Decrease RIFF chunk size

    } else if (new_duration > current_duration) {
        long bytes_done = data_section_bytes_copy(old_wav, new_wav, sizeof(wav_header), wav_header->subchunk2_size);
        long bytes_of_silence = target_bytes - bytes_done;
        uint16_t silence = 0;

        for (int i = 0; i < bytes_of_silence; i++) {  // Writing silence bytes to new file
            fwrite(&silence, sizeof(uint16_t), 1, new_wav);
        }

        new_subchunk2_size = wav_header->subchunk2_size + bytes_of_silence;  // Increase data chunk size
        new_chunk_size = 36 + new_subchunk2_size;                            // Increase RIFF chunk size
    }
    
    update_wav_header_sizes(new_wav, &new_chunk_size, &new_subchunk2_size);

    if (overwrite) {
        copy_all_file_contents(new_wav, old_wav);
        if (remove(new_filename) != 0) {
            fclose(old_wav);
            fclose(new_wav);
            free(original_filename);
            free(new_filename);
            perror("Error when removing temporary new file");
            return;
        }

        if (rename(file_path, new_filename) != 0) {
            perror("Error when renaming file");
        }
    } else {
        fclose(new_wav);
    }

    fclose(old_wav);
    free(original_filename);
    free(new_filename);
}
